﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class EditorUiScript : MonoBehaviour
{
    public GameObject BlockTemplate;
    public GameObject Cube;
    public Text CurrentBuildingText;

    public Text CurrentBlockText;
    public Text PosXText;
    public Text PosYText;
    public Text PosZText;
    public Text blockStateInfoText;

    private bool _texturesLoaded;
    private bool _texturesRendered;

    private readonly Dictionary<string, BlockModelInfo> _loadedModels = new Dictionary<string, BlockModelInfo>();
    private readonly Dictionary<string, Texture2D> _loadedTextures = new Dictionary<string, Texture2D>();
    private GameObject[,,] _instantiatedBBlocks;

    private string _blockStatesPath;

    private readonly Dictionary<string, string> _cachedModelFileNames = new Dictionary<string, string>();
    private readonly Dictionary<string, string> _cachedBsFilePaths = new Dictionary<string, string>();
    private static readonly int ShaderRenderMode = Shader.PropertyToID("_Mode");

    #region File Loading Functions

    public void PopulateModelFileName(BuildingBlock block, BuildingPlan plan, int x, int y, int z)
    {
        try
        {
            //Air has no texture.
            if (block.RegistryName == "minecraft:air") return;

            //Preprocess vanilla blocks - slabs, saplings, etc.
            var bsName = VanillaResourceLoader.PreprocessBlockstateIfNeeded(block, x, y, z, plan);

            //If we've already got this block's data, we don't need to get it again.
            if (_cachedModelFileNames.ContainsKey(block.ToString()))
            {
                block.ModelFileName = _cachedModelFileNames[block.ToString()];
                block.BlockStateFilePath = _cachedBsFilePaths[block.ToString()];
                return;
            }

            //Part 1: Locate blockstate file.
            string bsPath;
            Debug.Log("Attempting to locate blockstate file for block " + block.RegistryName +
                      " anew, as it wasn't found in the cache...");
            bsPath = block.RegistryName.StartsWith("minecraft:")
                ? VanillaResourceLoader.GetBlockStatePath(block, bsName, _blockStatesPath)
                : ModdedResourceLoader.GetBlockStatePath(block, bsName);

            if (bsPath == null)
            {
                Debug.LogWarning("Unable to find a blockstate file.");
                if (!_cachedBsFilePaths.ContainsKey(block.ToString()))
                    _cachedBsFilePaths.Add(block.ToString(), null);

                if (!_cachedModelFileNames.ContainsKey(block.ToString()))
                    _cachedModelFileNames.Add(block.ToString(), null);

                return;
            }

            Debug.Log("Attempting to load file " + bsPath);

            try
            {
                //Part 2: Load model information from blockstate file

                //Load the blockstate file
                var fileContent = File.ReadAllText(bsPath);
                Debug.Log("Successfully read file contents of file " + bsPath);
                block.BlockStateFilePath = bsPath;
                _cachedBsFilePaths.Add(block.ToString(), bsPath);

                //Parse it as JSON
                var blockStateData = JObject.Parse(fileContent);

                //If we have a variants object, then we need to filter it down based on the desired blockstate
                if (((IDictionary<string, JToken>) blockStateData).Keys.Contains("variants") && blockStateData
                        .Value<JObject>("variants") is JObject variants)
                {
                    JToken modelData = null;
                    var counter = 0;
                    while (modelData == null)
                    {
                        counter++;

                        //Infinite loop breakout in case something goes drastically wrong.
                        if (counter > 40)
                            throw new OverflowException("Tried 40 times to resolve " + block +
                                                        "'s model variant and failed.");

                        var validStates = new List<string>();

                        //Find all the blockstates that could match our desired one.
                        foreach (var state in ((IDictionary<string, JToken>) variants).Keys)
                        {
                            var stateList = state.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries);
                            var targetState = block.GetBlockstateData().Replace("{", "").Replace("}", "")
                                .Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries)
                                .Select(val => val.Trim().ToLower()).ToArray();
                            if (!stateList.ContainsAll(targetState)) continue;

                            validStates.Add(state);
                            break;
                        }

                        switch (validStates.Count)
                        {
                            //If we don't have ANY blockstates, we have to try to get the normal
                            case 0:
                                modelData = variants.Value<JToken>("normal");
                                break;
                            //If there's only one valid one, use it. Simple.
                            case 1:
                                modelData = variants.Value<JToken>(validStates[0]);
                                break;
                            default:
                            {
                                //Try to get halves, for doors etc.
                                var splits = validStates.Select(state => state.Split(','));
                                if (splits.Any(split => split.Any(kvp => kvp.StartsWith("half="))))
                                {
                                    var above = plan.Blocks[x, y + 1, z];
                                    block.BlockStateData.Add("half",
                                        above.RegistryName == block.RegistryName && above.Metadata == block.Metadata
                                            ? "lower"
                                            : "upper");
                                }
                                else
                                {
                                    //Fallback to the first valid one.
                                    modelData = variants.Value<JToken>(validStates[0]);
                                }

                                break;
                            }
                        }
                    }


                    //Sometimes we have an array here
                    if (modelData is JArray array)
                    {
                        modelData = array.Value<JObject>(0);
                    }

                    //Now we get the model filename
                    block.ModelFileName = modelData.Value<string>("model");
                    _cachedModelFileNames.Add(block.ToString(), block.ModelFileName);

                    //And any rotation details
                    try
                    {
                        block.Rotate.y = modelData.Value<int>("y");
                    }
                    catch (JsonException)
                    {
                    }

                    try
                    {
                        block.Rotate.x = modelData.Value<int>("x");
                    }
                    catch (JsonException)
                    {
                    }

                    try
                    {
                        block.Rotate.z = modelData.Value<int>("z");
                    }
                    catch (JsonException)
                    {
                    }
                }
                //Sometimes we have a multipart element, for stuff that's based on adjacent blocks, like glass panes, fences, etc.
                else if (((IDictionary<string, JToken>) blockStateData).Keys.Contains("multipart") &&
                         ((JObject) blockStateData.Value<JArray>("multipart")[0]).TryGetValue("apply",
                             out var modelData2))
                {
                    if (modelData2 is JArray array)
                    {
                        //TODO: In the future we may want to try to parse this for stuff like fences having accurate connections.
                        //But for now, just get the first value.
                        modelData2 = array.Value<JObject>(0);
                    }

                    block.ModelFileName = modelData2.Value<string>("model");
                    _cachedModelFileNames.Add(block.ToString(), block.ModelFileName);
                }
                else
                {
                    throw new ArgumentException("Blockstate does not contain a multipart or a variants element.");
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Unable to get model information for " + block + " => " + e);

                if (!_cachedBsFilePaths.ContainsKey(block.ToString()))
                    _cachedBsFilePaths.Add(block.ToString(), bsPath);

                if (!_cachedModelFileNames.ContainsKey(block.ToString()))
                    _cachedModelFileNames.Add(block.ToString(), block.ModelFileName);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Error resolving blockstate for " + block + " -> " + e);

            if (!_cachedBsFilePaths.ContainsKey(block.ToString()))
                _cachedBsFilePaths.Add(block.ToString(), null);

            if (!_cachedModelFileNames.ContainsKey(block.ToString()))
                _cachedModelFileNames.Add(block.ToString(), null);
        }
    }

    private static string GetFullTexturePath(string bsFilePath, BlockModelInfo.ModelElement element, string direction)
    {
        string partialPath;
        switch (direction)
        {
            case "up":
                partialPath = element.Up.Texture;
                break;
            case "down":
                partialPath = element.Down.Texture;
                break;
            case "north":
                partialPath = element.North.Texture;
                break;
            case "east":
                partialPath = element.East.Texture;
                break;
            case "south":
                partialPath = element.South.Texture;
                break;
            case "west":
                partialPath = element.West.Texture;
                break;
            default:
                return "";
        }

        if (partialPath.Length == 0) return null;

        return partialPath.Contains(":")
            ? ModdedResourceLoader.ResolveTextureFilePath(partialPath)
            : VanillaResourceLoader.ResolveTexturePath(partialPath);
    }

    private Texture2D GetOrLoadPngTexture(string path)
    {
        if (path == null) return null;

        if (_loadedTextures.ContainsKey(path)) return _loadedTextures[path];

        if (!File.Exists(path))
        {
            Debug.LogWarning("Texture not found: " + path);
            _loadedTextures.Add(path, Resources.Load<Texture2D>("missing_image"));
            return _loadedTextures[path];
        }

        Debug.Log("Loading Texture at " + path);

        var texture = new Texture2D(2, 2) {filterMode = FilterMode.Point};
        //Disable smoothing
        texture.LoadImage(File.ReadAllBytes(path));
        texture.Apply();

        _loadedTextures.Add(path, texture);
        return texture;
    }

    private void TextureFace(Transform face, Texture2D texture, BlockModelInfo.ModelElement.ModelFaceData faceData)
    {
        face.GetComponent<MeshRenderer>().material
            .mainTexture = texture;
        if(faceData.Rotation.HasValue)
            face.Rotate(face.forward, faceData.Rotation.Value);
                                
        if (faceData.Uv.FromX.HasValue)
        {
            //UV
            var differenceX = Math.Abs(faceData.Uv.ToX.Value - faceData.Uv.FromX.Value);
            var differenceY = Math.Abs(faceData.Uv.ToY.Value - faceData.Uv.FromY.Value);
            
            var scaleX = (float) differenceX / texture.width;
            var scaleY = (float) differenceY / texture.height;
            
            var offsetX = (float) faceData.Uv.FromX.Value / texture.width;
            var offsetY = (float) faceData.Uv.FromY.Value / texture.height;
                                    
            face.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
            face.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(offsetX, offsetY);
        }
        
    }

    #endregion

    // Start is called before the first frame update
    public void Start()
    {
        // Path Initialization
        ModdedResourceLoader.ModVerCachePath = Path.Combine(Application.persistentDataPath, "ModVerCache");
        VanillaResourceLoader.VanillaAssetsRoot =
            Path.Combine(Application.persistentDataPath, "GameVerCache", "1.12.2", "assets", "minecraft");

        // Handle accidentally landing on this scene
        if (CrossSceneState.CurrentPlan == null)
        {
            gameObject.SetActive(false);
            SceneManager.LoadScene("MenuScene");
            return;
        }

        //Development environment?
        if (CrossSceneState.CurrentPlan.Path.Contains(Path.Combine("content", "millenaire", "cultures")))
        {
            var rootDevFolder =
                CrossSceneState.CurrentPlan.Path.Substring(0, CrossSceneState.CurrentPlan.Path.IndexOf(
                                                                  Path.Combine("content", "millenaire", "cultures"),
                                                                  StringComparison.Ordinal) - 1);
            Debug.Log("Loaded plan is part of development environment. Loading development assets.");
            Debug.Log("Dev environment is located at " + rootDevFolder);

            var resourcesRoot = Path.Combine(rootDevFolder, "code", "resources");
            Debug.Log("Resources can be found at " + resourcesRoot);

            if (Directory.Exists(resourcesRoot))
                ModdedResourceLoader.RegisterPath("millenaire", resourcesRoot);
        }

        // Vanilla blockstates path
        _blockStatesPath = Path.Combine(Application.persistentDataPath, "GameVerCache", "1.12.2",
            "assets", "minecraft", "blockstates");


        #region Block Instantiation

        var plan = CrossSceneState.CurrentPlan;
        _instantiatedBBlocks = new GameObject[CrossSceneState.CurrentPlan.Width, CrossSceneState.CurrentPlan.NumFloors,
            CrossSceneState.CurrentPlan.Length];

        CurrentBuildingText.text = "Current Building: " + plan.Name;

        for (var floor = 0; floor < plan.NumFloors; floor++)
        {
            for (var x = 0; x < plan.Width; x++)
            {
                for (var z = 0; z < plan.Length; z++)
                {
                    var planBlock = plan.Blocks[x, floor, z];
                    if (planBlock.RegistryName == "minecraft:air")
                        continue;

                    var block = Instantiate(BlockTemplate);
                    block.SetActive(true);

                    block.transform.position = new Vector3(x, floor, z);
                    //block.transform.Rotate(0, 90, 0, Space.Self);
                    block.name = planBlock.PointName + "(" + planBlock.RegistryName + ":" + planBlock.Metadata + ")";

                    block.GetComponent<BlockScript>().Block = planBlock;
                    block.GetComponent<BlockScript>().XPos = x;
                    block.GetComponent<BlockScript>().YPos = floor;
                    block.GetComponent<BlockScript>().ZPos = z;

                    block.name = planBlock.RegistryName + " at " + x + ", " + floor + ", " + z;

                    _instantiatedBBlocks[x, floor, z] = block;
                }
            }
        }

        #endregion
    }

    // Update is called once per frame
    public void Update()
    {
        #region Model Loading

        var vanillaModelsPath = Path.Combine(Application.persistentDataPath, "GameVerCache", "1.12.2", "assets",
            "minecraft",
            "models");
        if (!_texturesLoaded)
        {
            //If we've invalidated loaded textures, re-render too.
            _texturesRendered = false;

            //Initialize models
            //Models must be loaded bottom-up
            var plan = CrossSceneState.CurrentPlan;
            for (var floor = 0; floor < plan.NumFloors; floor++)
            {
                for (var x = 0; x < plan.Width; x++)
                {
                    for (var z = 0; z < plan.Length; z++)
                    {
                        var buildingBlock = plan.Blocks[x, floor, z];

                        //Try to resolve the model filename
                        PopulateModelFileName(buildingBlock, plan, x, floor, z);

                        //If we fail, ignore.
                        if (buildingBlock.ModelFileName == null) continue;

                        try
                        {
                            if (_loadedModels.ContainsKey(buildingBlock.ModelFileName)) continue;

                            Debug.Log("Need to load model: " + buildingBlock.ModelFileName +
                                      " as it's not in the cache.");

                            string modelFileText;
                            try
                            {
                                //Try to quick-load the vanilla model
                                modelFileText =
                                    File.ReadAllText(Path.Combine(vanillaModelsPath,
                                        buildingBlock.ModelFileName + ".json"));
                            }
                            catch (IOException)
                            {
                                //Otherwise use the blockstate file location as a base, go up one directory then models/blocks
                                if (buildingBlock.ModelFileName.Contains(":"))
                                {
                                    //Modded
                                    modelFileText =
                                        File.ReadAllText(
                                            ModdedResourceLoader.ResolveModelFilePath(buildingBlock.ModelFileName));
                                }
                                else
                                    //Vanilla
                                    modelFileText = File.ReadAllText(Path.Combine(
                                        Directory.GetParent(Path.GetDirectoryName(buildingBlock.BlockStateFilePath)
                                        ).FullName, "models", "block", buildingBlock.ModelFileName + ".json"));
                            }

                            Debug.Log("Successfully located and loaded contents of file " +
                                      buildingBlock.ModelFileName);

                            //Parse the model file
                            var modelData = JObject.Parse(modelFileText);

                            //Load all parents and merge their data into the model data.
                            while (((IDictionary<string, JToken>) modelData).Keys.Contains("parent"))
                            {
                                var parentFilename = modelData.Value<string>("parent");
                                Debug.Log("\tLoading Parent Model " + parentFilename);
                                parentFilename = parentFilename.Contains(":")
                                    ? ModdedResourceLoader.ResolveModelFilePath(parentFilename)
                                    : Path.Combine(vanillaModelsPath, parentFilename + ".json");

                                modelData.Remove("parent");
                                modelData.Merge(
                                    JObject.Parse(
                                        File.ReadAllText(parentFilename)));
                            }

                            //Initialize our elements list.
                            var elements = new List<BlockModelInfo.ModelElement>();

                            //Populate elements
                            foreach (var e in modelData.Value<JArray>("elements"))
                            {
                                var element = (JObject) e;
                                var from = element.Value<JArray>("from");
                                var to = element.Value<JArray>("to");

                                var elem = new BlockModelInfo.ModelElement
                                {
                                    BlockStart =
                                        new Vector3(from.Value<int>(0), from.Value<int>(1), from.Value<int>(2)) / 16,
                                    BlockEnd = new Vector3(to.Value<int>(0), to.Value<int>(1), to.Value<int>(2)) / 16,
                                    Up = BlockModelInfo.ModelElement.ModelFaceData.ReadFromJson(modelData, element.Value<JObject>("faces").Value<JObject>("up")),
                                    Down = BlockModelInfo.ModelElement.ModelFaceData.ReadFromJson(modelData, element.Value<JObject>("faces").Value<JObject>("down")),
                                    North = BlockModelInfo.ModelElement.ModelFaceData.ReadFromJson(modelData, element.Value<JObject>("faces").Value<JObject>("north")),
                                    East = BlockModelInfo.ModelElement.ModelFaceData.ReadFromJson(modelData, element.Value<JObject>("faces").Value<JObject>("east")),
                                    South = BlockModelInfo.ModelElement.ModelFaceData.ReadFromJson(modelData, element.Value<JObject>("faces").Value<JObject>("south")),
                                    West = BlockModelInfo.ModelElement.ModelFaceData.ReadFromJson(modelData, element.Value<JObject>("faces").Value<JObject>("west")),
                                };

                                if (((IDictionary<string, JToken>) element).Keys.Contains("rotation") &&
                                    element.Value<JToken>("rotation") is JObject rotation)
                                {
                                    var originArray = rotation.Value<JArray>("origin");
                                    var axis = rotation.Value<string>("axis");
                                    var angle = rotation.Value<float>("angle");

                                    elem.Rotation = new BlockModelInfo.ModelElement.ModelElementRotation
                                    {
                                        Origin = new Vector3(originArray.Value<float>(0), originArray.Value<float>(1),
                                                     originArray.Value<float>(2)) / 16,
                                        Angle = angle,
                                        Axis = axis == "z" ? Vector3.forward : axis == "x" ? Vector3.right : Vector3.up,
                                    };
                                }

                                elements.Add(elem);
                            }


                            //Initialize model information object
                            var modelInfo = new BlockModelInfo
                            {
                                Name = buildingBlock.ModelFileName
                            };

                            //And populate elements
                            modelInfo.Elements.AddRange(elements);

                            Debug.Log("Successfully loaded model information for model file " +
                                      buildingBlock.ModelFileName +
                                      " with " +
                                      modelInfo.Elements.Count + " elements.");

                            //Cache it.
                            _loadedModels.Add(buildingBlock.ModelFileName, modelInfo);
                            return; //Load at most one model per frame anew.
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("Exception loading model file " + buildingBlock.ModelFileName +
                                           " for block " +
                                           buildingBlock + " => " + e);
                            _loadedModels[buildingBlock.ModelFileName] = null;
                            return;
                        }
                    }
                }
            }

            Debug.Log("Loaded " + _loadedModels.Count + " models.");
            _texturesLoaded = true;
        }
        else if (!_texturesRendered)
        {
            //Render the textures

            var plan = CrossSceneState.CurrentPlan;

            for (var floor = 0; floor < plan.NumFloors; floor++)
            {
                for (var x = 0; x < plan.Width; x++)
                {
                    for (var z = 0; z < plan.Length; z++)
                    {
                        var planBlock = plan.Blocks[x, floor, z];
                        var bBlock = _instantiatedBBlocks[x, floor, z];
                        if (planBlock.ModelFileName == null || !_loadedModels.ContainsKey(planBlock.ModelFileName) ||
                            _loadedModels[planBlock.ModelFileName] == null)
                        {
                            if (planBlock.RegistryName == "minecraft:air") continue;

                            var missingCube = Instantiate(Cube, bBlock.transform, false);
                            var texture = Resources.Load<Texture2D>("missing_image");

                            foreach (var childRenderer in missingCube.transform.GetComponentsInChildren<MeshRenderer>())
                            {
                                childRenderer.material.mainTexture = texture;
                            }

                            continue;
                        }

                        var model = _loadedModels[planBlock.ModelFileName];

                        foreach (var element in model.Elements)
                        {
                            var elementCubePivot = Instantiate(Cube, bBlock.transform, false);

                            var up = GetOrLoadPngTexture(
                                GetFullTexturePath(planBlock.BlockStateFilePath, element, "up"));
                            var down = GetOrLoadPngTexture(
                                GetFullTexturePath(planBlock.BlockStateFilePath, element, "down"));
                            var north = GetOrLoadPngTexture(
                                GetFullTexturePath(planBlock.BlockStateFilePath, element, "north"));
                            var east = GetOrLoadPngTexture(
                                GetFullTexturePath(planBlock.BlockStateFilePath, element, "east"));
                            var south = GetOrLoadPngTexture(
                                GetFullTexturePath(planBlock.BlockStateFilePath, element, "south"));
                            var west = GetOrLoadPngTexture(
                                GetFullTexturePath(planBlock.BlockStateFilePath, element, "west"));

                            if (up == null)
                                elementCubePivot.transform.Find("Cube/Top").gameObject.SetActive(false);
                            else
                            {
                                TextureFace(elementCubePivot.transform.Find("Cube/Top"), up, element.Up);
                            }

                            if (down == null)
                                elementCubePivot.transform.Find("Cube/Bottom").gameObject.SetActive(false);
                            else
                            {
                                TextureFace(elementCubePivot.transform.Find("Cube/Bottom"), down, element.Down);
                            }

                            if (north == null)
                                elementCubePivot.transform.Find("Cube/Back").gameObject.SetActive(false);
                            else
                            {
                                TextureFace(elementCubePivot.transform.Find("Cube/Back"), north, element.North);
                            }

                            if (east == null)
                                elementCubePivot.transform.Find("Cube/Right").gameObject.SetActive(false);
                            else
                            {
                                TextureFace(elementCubePivot.transform.Find("Cube/Right"), east, element.East);

                            }

                            if (south == null)
                                elementCubePivot.transform.Find("Cube/Front").gameObject.SetActive(false);
                            else
                            {
                                TextureFace(elementCubePivot.transform.Find("Cube/Front"), south, element.South);

                            }

                            if (west == null)
                                elementCubePivot.transform.Find("Cube/Left").gameObject.SetActive(false);
                            else
                            {
                                TextureFace(elementCubePivot.transform.Find("Cube/Left"), west, element.West);
                            }

                            elementCubePivot.transform.localPosition = element.BlockStart;
                            elementCubePivot.transform.localScale = element.BlockEnd - element.BlockStart;

                            if (element.Rotation != null)
                            {
                                elementCubePivot.transform.RotateAround(
                                    elementCubePivot.transform.position + element.Rotation.Origin,
                                    element.Rotation.Axis, element.Rotation.Angle);
                            }
                        }

                        //X rotate
                        bBlock.transform.RotateAround(bBlock.GetComponent<BoxCollider>().bounds.center, Vector3.left,
                            planBlock.Rotate.x);

                        //Y rotate
                        bBlock.transform.RotateAround(bBlock.GetComponent<BoxCollider>().bounds.center, Vector3.up,
                            planBlock.Rotate.y + 90);

                        //Z rotate
                        bBlock.transform.RotateAround(bBlock.GetComponent<BoxCollider>().bounds.center, Vector3.back,
                            planBlock.Rotate.z);
                    }
                }
            }

            _texturesRendered = true;
        }

        #endregion

        #region Movement with Keyboard

        var horizontal = Input.GetAxisRaw("Horizontal");
        var centerOfBuilding = Vector3.zero +
                               new Vector3(CrossSceneState.CurrentPlan.Width / 2f, 0,
                                   CrossSceneState.CurrentPlan.Length / 2f);
        if (horizontal < 0)
        {
            //Left
            Camera.main.transform.position -= (Camera.main.transform.right * 0.1f);
        }
        else if (horizontal > 0)
        {
            //Right
            Camera.main.transform.position += (Camera.main.transform.right * 0.1f);
        }

        var vertical = Input.GetAxisRaw("Vertical");
        if (vertical < 0)
        {
            //Backwards
            Camera.main.transform.position -= (Camera.main.transform.forward * 0.1f);
        }
        else if (vertical > 0)
        {
            //Forwards
            Camera.main.transform.position += (Camera.main.transform.forward * 0.1f);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            //Rotate left
            Camera.main.transform.Rotate(Vector3.forward, 0.5f);
            //Camera.main.transform.RotateAround(centerOfBuilding, Vector3.up, 1f);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            //Rotate right
            Camera.main.transform.Rotate(Vector3.forward, -0.5f);
            //Camera.main.transform.RotateAround(centerOfBuilding, Vector3.up, -1f);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            //Up
            Camera.main.transform.position += (Vector3.up * 0.1f);
        }
        else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            //Down
            Camera.main.transform.position += (Vector3.down * 0.1f);
        }

        var scroll = Input.GetAxisRaw("Mouse ScrollWheel");
        if (scroll > 0)
        {
            //Scroll up
            Debug.Log("In");
        }
        else if (scroll < 0)
        {
            //Scroll down
            Debug.Log("Out");
        }

        #endregion

        //Drag handling

//        if (Input.GetMouseButtonDown(1))
//        {
//            //Right click
//            dragOrigin = Input.mousePosition;
//        }

        if (!Input.GetMouseButton(1)) return;

        var mouseHorizontal = Input.GetAxis("Mouse X");
        var mouseVertical = Input.GetAxis("Mouse Y");

        //Rotate around the up axis for left(-) and right(+)

        //So we need to rotate around up by the negative x of the drag
        Camera.main.transform.Rotate(Vector3.up, mouseHorizontal * 4);

        //Rotating around the Z (forward) axis tilts the camera right and left

        //Rotate around X (right) axis for up(-) and down(+) movement
        //So we need to rotate around right by negative y of the drag
        Camera.main.transform.Rotate(Vector3.left, (mouseVertical * 4));
    }

    public void UpdateRightPanel()
    {
        if (CrossSceneState.SelectedBlock is BlockScript block)
        {
            CurrentBlockText.text = "Selected Block: " + block.Block.DisplayName + ":" + block.Block.Metadata;
            PosXText.text = "Pos X: " + block.XPos;
            PosYText.text = "Pos Y: " + block.YPos;
            PosZText.text = "Pos Z: " + block.ZPos;
        }
        else
        {
            CurrentBlockText.text = "No Block Selected";
            PosXText.text = "Pos X: ";
            PosYText.text = "Pos Y: ";
            PosZText.text = "Pos Z: ";
        }
    }
}