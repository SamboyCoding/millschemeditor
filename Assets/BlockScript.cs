﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BlockScript : MonoBehaviour
{
    public BuildingBlock Block;
    public int XPos;
    public int YPos;
    public int ZPos;

    public void OnMouseDown()
    {
        //Click
        CrossSceneState.SelectedBlock = this;

        var vertices = GetComponent<MeshFilter>().mesh.vertices;

        foreach (var sphere in GameObject.FindGameObjectsWithTag("CORNER_SPHERE"))
        {
            sphere.SetActive(false);
        }

        foreach (Transform child in transform)
        {
            if (child.gameObject.CompareTag("CORNER_SPHERE"))
            {
                child.gameObject.SetActive(true);
            }
        }

        CrossSceneState.SelectedBlock = this;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    public void Update()
    {
//        if (_spheres.Length > 0 && CrossSceneState.SelectedBlock != this)
//        {
//            Debug.Log("Removing my spheres");
//            //Remove spheres
//            foreach (var sphere in _spheres)
//            {
//                Destroy(sphere);
//            }
//
//            _spheres = new GameObject[0];
//        }
    }
}