using System;
using System.IO;
using System.Linq;

public static class StreamUtil
{
    public static int Read4ByteInt(Stream s)
    {
        var bytes = new byte[4];
        s.Read(bytes, 0, 4);
        Array.Reverse(bytes); //Data is big-endian, so reverse.
        return BitConverter.ToInt32(bytes, 0);
    }

    public static string ReadBytesAsString(Stream s, int amount)
    {
        var bytes = new byte[amount];
        s.Read(bytes, 0, amount);
        return new string(bytes.Select(b => (char) b).ToArray());
    }
}