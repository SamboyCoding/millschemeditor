using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ArrayUtils
{
    public static bool ContainsAll(this IEnumerable<string> container, IEnumerable<string> mustContain)
    {
        return mustContain.All(elem => container.Contains(elem.Trim().ToLower()));
    }
}