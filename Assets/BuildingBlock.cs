using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingBlock
{
    public string PointName;
    public string DisplayName;
    public string RegistryName;
    public short Metadata;
    public Dictionary<string, string> BlockStateData = new Dictionary<string, string>();
    public string BlockStateFilePath;
    public string ModelFileName;
    public Vector3 Rotate = Vector3.zero;

    public BuildingBlock(string pn, string bn, short m)
    {
        PointName = pn;
        RegistryName = bn.Length > 0 ? bn : "minecraft:air";
        Metadata = m;
        
        //Specific overrides, such a preserveground
        switch (PointName)
        {
            case "preserveground":
                DisplayName = "millenaire:preserveground";
                RegistryName = "minecraft:dirt";
                break;
            case "grass":
                DisplayName = "millenaire:special_grass";
                RegistryName = "minecraft:grass";
                BlockStateData.Add("snowy", "false");
                break;
            default:
                DisplayName = RegistryName;
                break;
        }
    }
    
    public BuildingBlock(string bn, short m): this("", bn, m) {}

    public BuildingBlock(string bn) : this(bn, 0) {}

    public string GetBlockstateData()
    {
        if (BlockStateData.Count == 0) return "{}";
        var s = "{";

        foreach (var kvp in BlockStateData)
        {
            s += kvp.Key + "=" + kvp.Value + ",";
        }

        return s.Substring(0, s.Length - 1) + "}";
    }

    public override string ToString()
    {
        return RegistryName + ":" + Metadata + " " + GetBlockstateData();
    }
}