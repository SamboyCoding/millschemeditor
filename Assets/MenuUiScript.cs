﻿using System;
using System.IO;
using System.Linq;
using Ionic.Zip;
using SFB;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Task = System.Threading.Tasks.Task;

public class MenuUiScript : MonoBehaviour
{
    public GameObject Panel;

    public Text LoadingText;

    //Main Menu
    public Button CreateNewButton;
    public Button OpenBuildingButton;
    public Button QuitButton;

    //Png blockmap Submenu
    public Text BlockMapInfoText;
    public Button LoadBlockmapButton;
    public Button AbortLoadButton;
    public Text SuggestedBlockmapText;
    public Button UseSuggestedMapButton;

    private string _buildingFilePath = "";
    private string _blockmapFilePath = "";
    private string _suggestedBlockmapFilePath = "";

    // Use this for initialization
    public void Start()
    {
        OpenBuildingButton.onClick.AddListener(OnOpenBuildingClicked);
        QuitButton.onClick.AddListener(DoQuit);
        AbortLoadButton.onClick.AddListener(ReturnToMenu);
        LoadBlockmapButton.onClick.AddListener(DoLoadBlockmap);
        UseSuggestedMapButton.onClick.AddListener(UseSuggestedBlockmap);

        //Let's try to find the game installation
        string baseSearchDir;
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                //On windows
                baseSearchDir = Path.Combine(Environment.GetEnvironmentVariable("APPDATA"), ".minecraft");
                break;
            case RuntimePlatform.LinuxEditor:
            case RuntimePlatform.LinuxPlayer:
                baseSearchDir = "~/.minecraft";
                //On linux
                break;
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
                baseSearchDir = "~/Library/Application Support/minecraft";
                //On mac
                break;
            default:
                Debug.LogError("How exactly are we running on " + Application.platform + "?");
                return;
        }

        if (Directory.Exists(baseSearchDir) && Directory.Exists(Path.Combine(baseSearchDir, "versions", "1.12.2")))
        {
            CrossSceneState.GameJarPath =
                Path.Combine(baseSearchDir, "versions", "1.12.2",
                    "1.12.2.jar"); //TODO: Make this user-facing and editable.
            Debug.Log("Located minecraft at " + CrossSceneState.GameJarPath);

            var path = Path.Combine(Application.persistentDataPath, "GameVerCache", "1.12.2");
            Task.Run(() =>
            {
                Debug.Log("Beginning Game file extract thread");
                if (!Directory.Exists(path))
                {
                    Debug.Log("Extracting game jar...");
                    Directory.CreateDirectory(path);
                    using (var gameJar = ZipFile.Read(CrossSceneState.GameJarPath))
                    {
                        var assets = from e in gameJar.Entries
                            where e.FileName.StartsWith("assets/minecraft/models") || e.FileName.StartsWith("assets/minecraft/textures") || e.FileName.StartsWith("assets/minecraft/blockstates")
                            select e;

                        foreach (var asset in assets)
                            asset.Extract(path);
                    }

                    Debug.Log("Extracted game files to " + path);
                }
                else
                {
                    Debug.Log("Nothing to do.");
                }
                Debug.Log("Game file extraction complete.");
            });
        }
        else
        {
            Debug.LogWarning("Couldn't find minecraft jar!");
        }
    }

    private void UseSuggestedBlockmap()
    {
        if (_suggestedBlockmapFilePath.Length < 1) return;

        _blockmapFilePath = _suggestedBlockmapFilePath;

        LoadingText.gameObject.SetActive(true);

        BlockMapInfoText.gameObject.SetActive(false);
        LoadBlockmapButton.gameObject.SetActive(false);
        AbortLoadButton.gameObject.SetActive(false);
        UseSuggestedMapButton.gameObject.SetActive(false);

        CrossSceneState.CurrentPlan = PngPlanReader.ReadFromFile(_buildingFilePath, _blockmapFilePath);

        SceneManager.LoadSceneAsync("EditorScene");
    }

    private void DoLoadBlockmap()
    {
        var paths = StandaloneFileBrowser.OpenFilePanel("Select Blockmap File", "",
            new[] {new ExtensionFilter("Text Files", "txt")}, false);

        //Don't continue if the user cancelled the dialog.
        if (paths == null || paths.Length < 1) return;

        BlockMapInfoText.gameObject.SetActive(false);
        LoadBlockmapButton.gameObject.SetActive(false);
        AbortLoadButton.gameObject.SetActive(false);
        UseSuggestedMapButton.gameObject.SetActive(false);

        LoadingText.gameObject.SetActive(true);

        _blockmapFilePath = paths[0];

        //Read the plan
        CrossSceneState.CurrentPlan = PngPlanReader.ReadFromFile(_buildingFilePath, _blockmapFilePath);

        SceneManager.LoadSceneAsync("EditorScene");
    }

    private void ReturnToMenu()
    {
        CreateNewButton.gameObject.SetActive(true);
        OpenBuildingButton.gameObject.SetActive(true);
        QuitButton.gameObject.SetActive(true);

        BlockMapInfoText.gameObject.SetActive(false);
        LoadBlockmapButton.gameObject.SetActive(false);
        AbortLoadButton.gameObject.SetActive(false);

        LoadingText.gameObject.SetActive(false);
        
        UseSuggestedMapButton.gameObject.SetActive(false);
        SuggestedBlockmapText.gameObject.SetActive(false);

        _buildingFilePath = string.Empty;
        //_blockmapFilePath = string.Empty;
    }

    private static void DoQuit()
    {
        Application.Quit();
    }

    private void OnOpenBuildingClicked()
    {
        var paths = StandaloneFileBrowser.OpenFilePanel("Select Building File", "",
            new[] {new ExtensionFilter("PNG Building Files", "png")}, false); //TODO: Schematic files.

        //Don't continue if the user cancelled the operation.
        if (paths == null || paths.Length < 1) return;

        _buildingFilePath = paths[0];

        //We have a file, now if it's a PNG we need a blockmap.

        if (_buildingFilePath.EndsWith(".png"))
        {
            //Show png loading info
            CreateNewButton.gameObject.SetActive(false);
            OpenBuildingButton.gameObject.SetActive(false);
            QuitButton.gameObject.SetActive(false);

            LoadingText.gameObject.SetActive(true);

            //Try to find the blockmap in the parent directories
            var path = _buildingFilePath;
            while (Directory.GetParent(path) != null)
            {
                path = Directory.GetParent(path).FullName;
                if (File.Exists(Path.Combine(path, "blocklist.txt")))
                {
                    _suggestedBlockmapFilePath = Path.Combine(path, "blocklist.txt");
                    break;
                }
            }

            LoadingText.gameObject.SetActive(false);
            BlockMapInfoText.gameObject.SetActive(true);
            LoadBlockmapButton.gameObject.SetActive(true);
            AbortLoadButton.gameObject.SetActive(true);

            if (_suggestedBlockmapFilePath.Length > 0)
            {
                SuggestedBlockmapText.text = "Alternatively, we've found what appears to be a blockmap at " +
                                             _suggestedBlockmapFilePath + ".\nClick here to use it:";
                SuggestedBlockmapText.gameObject.SetActive(true);
                UseSuggestedMapButton.gameObject.SetActive(true);
            }
            else
            {
                SuggestedBlockmapText.gameObject.SetActive(false);
                UseSuggestedMapButton.gameObject.SetActive(false);
            }

            return;
        }

        //TODO: Attempt to load schematic.
    }

    // Update is called once per frame
    void Update()
    {
    }
}