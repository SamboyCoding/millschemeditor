using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class VanillaResourceLoader
{
    public static string VanillaAssetsRoot;

    private static readonly Dictionary<int, string> metaColors = new Dictionary<int, string>
    {
        {0, "white"},
        {1, "orange"},
        {2, "magenta"},
        {3, "light_blue"},
        {4, "yellow"},
        {5, "lime"},
        {6, "pink"},
        {7, "gray"},
        {8, "light_gray"},
        {9, "cyan"},
        {10, "purple"},
        {11, "blue"},
        {12, "brown"},
        {13, "green"},
        {14, "red"},
        {15, "black"}
    };

    public static string PreprocessBlockstateIfNeeded(BuildingBlock block, int x, int y, int z, BuildingPlan plan)
    {
        string blockStateName = null;
        if (block.RegistryName.StartsWith("minecraft"))
        {
            #region Wooden slab specific handling. (Ew)

            if (block.RegistryName == "minecraft:wooden_slab")
            {
                switch (block.Metadata)
                {
                    case 0:
                        blockStateName = "oak_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 1:
                        blockStateName = "spruce_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 2:
                        blockStateName = "birch_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 3:
                        blockStateName = "jungle_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 4:
                        blockStateName = "acacia_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 5:
                        blockStateName = "dark_oak_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 8:
                        blockStateName = "oak_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 9:
                        blockStateName = "spruce_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 10:
                        blockStateName = "birch_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 11:
                        blockStateName = "jungle_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 12:
                        blockStateName = "acacia_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 13:
                        blockStateName = "dark_oak_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                }
            }

            #endregion

            #region And Stone slab (also ew)

            if (block.RegistryName == "minecraft:stone_slab")
            {
                switch (block.Metadata)
                {
                    case 0:
                        blockStateName = "stone_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 1:
                        blockStateName = "sandstone_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 2:
                        blockStateName = "wood_old_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 3:
                        blockStateName = "cobblestone_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 4:
                        blockStateName = "brick_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 5:
                        blockStateName = "stone_brick_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 6:
                        blockStateName = "nether_brick_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 7:
                        blockStateName = "quartz_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "bottom");
                        break;
                    case 8:
                        blockStateName = "stone_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 9:
                        blockStateName = "sandstone_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 10:
                        blockStateName = "wood_old_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 11:
                        blockStateName = "cobblestone_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 12:
                        blockStateName = "brick_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 13:
                        blockStateName = "stone_brick_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 14:
                        blockStateName = "nether_brick_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                    case 15:
                        blockStateName = "quartz_slab";
                        if (!block.BlockStateData.ContainsKey("half"))
                            block.BlockStateData.Add("half", "top");
                        break;
                }
            }

            #endregion

            #region Double Stone Slab

            if (block.RegistryName == "minecraft:double_stone_slab")
            {
                switch (block.Metadata)
                {
                    case 0:
                        blockStateName = "stone_double_slab";
                        break;
                    case 1:
                        blockStateName = "sandstone_double_slab";
                        break;
                    case 2:
                        blockStateName = "wood_old_double_slab";
                        break;
                    case 3:
                        blockStateName = "brick_double_slab";
                        break;
                    case 4:
                        blockStateName = "stone_brick_double_slab";
                        break;
                    case 5:
                        blockStateName = "quartz_double_slab";
                        break;
                    case 6:
                        blockStateName = "nether_brick_double_slab";
                        break;
                }
            }

            #endregion

            #region Doors

            if (block.RegistryName.EndsWith("_door"))
            {
                if (block.BlockStateData.ContainsKey("half") && block.BlockStateData["half"] == "upper" &&
                    plan.Blocks[x, y - 1, z].RegistryName == block.RegistryName)
                {
                    //Take facing from below block
                    if (!block.BlockStateData.ContainsKey("facing"))
                        block.BlockStateData.Add("facing", plan.Blocks[x, y - 1, z].BlockStateData["facing"]);
                }
                else
                {
                    if (!block.BlockStateData.ContainsKey("half"))
                        block.BlockStateData.Add("half", "lower");

                    if (plan.Blocks[x, y + 1, z].RegistryName == block.RegistryName)
                    {
                        //Lower half, take hinge direction from above if present
                        if (!block.BlockStateData.ContainsKey("hinge") &&
                            plan.Blocks[x, y + 1, z].BlockStateData.ContainsKey("hinge"))
                            block.BlockStateData.Add("hinge", plan.Blocks[x, y + 1, z].BlockStateData["hinge"]);
                    }
                }
            }

            #endregion
        }

        if (blockStateName != null)
        {
            Debug.Log("Preprocessed " + block + " => " + blockStateName);
        }

        //If we don't have a name yet, take the one from the block id.
        blockStateName = blockStateName ??
                         block.RegistryName.Substring(block.RegistryName.IndexOf(":", StringComparison.Ordinal) + 1);

        if (blockStateName.EndsWith("2"))
        {
            //Handle leaves2 and log2
            blockStateName = blockStateName.Substring(0, blockStateName.Length - 1);
        }

        if (block.BlockStateData.ContainsKey("variant") && block.RegistryName != "minecraft:dirt" && (block.RegistryName != "minecraft:stone" || block.BlockStateData["variant"] != "stone"))
        {
            //Logs, saplings, etc
            blockStateName = block.BlockStateData["variant"] + "_" + blockStateName;
            block.BlockStateData.Remove("variant");
        }
        else if (block.BlockStateData.ContainsKey("type"))
        {
            //Slabs?
            blockStateName = block.BlockStateData["type"] + "_" + blockStateName;
        }

        return blockStateName;
    }

    public static string GetBlockStatePath(BuildingBlock block, string preprocessedName, string bsParentPath)
    {
        //If the filename is already correct, use that.
        if (File.Exists(Path.Combine(bsParentPath, preprocessedName + ".json")))
            return Path.Combine(bsParentPath, preprocessedName + ".json");


        //Podzol, dirt, etc.
        if (block.BlockStateData.ContainsKey("kind")
            && File.Exists(Path.Combine(bsParentPath, block.BlockStateData["kind"] + ".json")))
        {
            return Path.Combine(bsParentPath, block.BlockStateData["kind"] + ".json");
        }

        //Weird case, not sure if ever used.
        if (block.BlockStateData.ContainsKey("variant")
            && File.Exists(Path.Combine(bsParentPath, block.BlockStateData["variant"] + ".json")))
        {
            return Path.Combine(bsParentPath, block.BlockStateData["variant"] + ".json");
        }

        //Coloured blocks - wool, carpet, concrete, etc.
        if (File.Exists(Path.Combine(bsParentPath, metaColors[block.Metadata] + "_" + preprocessedName + ".json")))
        {
            return Path.Combine(bsParentPath, metaColors[block.Metadata] + "_" + preprocessedName + ".json");
        }

        Debug.LogWarning("Failed to load blockstate: " + preprocessedName + " for block " + block + " as the blockstate file doesn't exist.");
        return null;
    }

    public static string ResolveTexturePath(string partialPath)
    {
        if (File.Exists(Path.Combine(VanillaAssetsRoot, partialPath + ".png")))
            return Path.Combine(VanillaAssetsRoot, partialPath + ".png");
        if (File.Exists(Path.Combine(VanillaAssetsRoot, "textures", partialPath + ".png")))
            return Path.Combine(VanillaAssetsRoot, "textures", partialPath + ".png");

        return Path.Combine(VanillaAssetsRoot, "textures", "blocks", partialPath + ".png");
    }
}