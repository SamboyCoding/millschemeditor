using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class BlockModelInfo
{
    public string Name;
    public readonly List<ModelElement> Elements = new List<ModelElement>();

    public class ModelElement
    {
        //From
        public Vector3 BlockStart = Vector3.zero;
        
        //To
        public Vector3 BlockEnd = Vector3.one;

        //Faces.up
        public ModelFaceData Up;
        //Faces.down
        public ModelFaceData Down;
        //Faces.north
        public ModelFaceData North;
        //Faces.south
        public ModelFaceData South;
        //Faces.east
        public ModelFaceData East;
        //Faces.west
        public ModelFaceData West;
        
        //rotation
        public ModelElementRotation Rotation = null;
        
        private static string ResolveTextureValue(JObject modelData, string requestedKey)
        {
            if (requestedKey == null) return "";
            var value = requestedKey;
            var counter = 0;
            while (true)
            {
                if (counter > 40)
                    throw new ArgumentException("Iterated more than 40 times, still couldn't get value for key " +
                                                requestedKey);
                if (!value.StartsWith("#")) return value;


                var tempValue = modelData.Value<JObject>("textures").Value<string>(value.Substring(1));
                if (tempValue == null)
                {
                    Debug.LogError("Failed to resolve texture value, as " + value + " has a value of null.");
                    return "";
                }

                value = tempValue;
                counter++;
            }
        }

        public class ModelElementRotation
        {
            public Vector3 Origin;
            public Vector3 Axis;
            public float Angle;
        }

        public class ModelFaceUVData
        {
            public int? FromX;
            public int? FromY;
            public int? ToX;
            public int? ToY;
        }

        public class ModelFaceData
        {
            public ModelFaceUVData Uv;
            public string Texture;
            public int? Rotation;
            
            public static ModelFaceData ReadFromJson(JObject modelData, JObject face)
            {
                return new ModelFaceData
                {
                    Texture = ResolveTextureValue(modelData, face?.Value<string>("texture")),
                    Uv = new ModelFaceUVData
                    {
                        FromX = face?
                            .Value<JArray>("uv")?[0].Value<int>(),
                        FromY = face?
                            .Value<JArray>("uv")?[1].Value<int>(),
                        ToX = face?
                            .Value<JArray>("uv")?[2].Value<int>(),
                        ToY = face?
                            .Value<JArray>("uv")?[3].Value<int>(),
                    },
                    Rotation = face?.Value<int>("rotation")

                };
            }
        }
    }
}