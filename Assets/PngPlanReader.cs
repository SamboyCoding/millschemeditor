using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Ionic.Zlib;
using UnityEngine;
using CompressionMode = Ionic.Zlib.CompressionMode;
using DeflateStream = Ionic.Zlib.DeflateStream;

public static class PngPlanReader
{
    #region Array Helper Functions

    public static T[] SubArray<T>(this T[] data, int index, int length)
    {
        T[] result = new T[length];
        Array.Copy(data, index, result, 0, length);
        return result;
    }

    public static T[] Reverse<T>(this T[] data)
    {
        T[] result = new T[data.Length];
        Array.Copy(data, 0, result, 0, data.Length);
        Array.Reverse(result);
        return result;
    }

    #endregion

    private static byte[] lastIdat;

    private static RgbPixel[,] ReadPng(string path)
    {
        if (!File.Exists(path)) throw new ArgumentException("Specified file does not exist!");

        var chunks = new Dictionary<string, PngChunk>();

        //And now follows an actual PNG file reading solution because unity doesn't have one. Yay.

        //Read the file chunks
        using (var stream = File.OpenRead(path))
        {
            //Check the header is right.
            if (new byte[] {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A}.Any(b => stream.ReadByte() != b))
                throw new ArgumentException("Not a PNG file!");


            var read = true;
            while (read)
            {
                var length = StreamUtil.Read4ByteInt(stream);
                var type = StreamUtil.ReadBytesAsString(stream, 4);

                var data = new byte[length];
                stream.Read(data, 0, length);

                var crc = StreamUtil.Read4ByteInt(stream);

                if (!chunks.ContainsKey(type))
                    chunks.Add(type, new PngChunk
                    {
                        Checksum = crc,
                        Data = data,
                        Length = length,
                        Type = type
                    });

                if (type == "IEND") read = false;
            }
        }

        //Check we have both necessary chunks - the header and main data
        if (!chunks.ContainsKey("IHDR")) throw new ArgumentException("Missing Image Header!");
        if (!chunks.ContainsKey("IDAT")) throw new ArgumentException("Missing Image Data!");

        //Image data is compressed using a Zlib stream, so decompress that.
        var imageData = ZlibStream.UncompressBuffer(chunks["IDAT"].Data);
        lastIdat = imageData;

        //Image data has R G B R G B R G B so on - three bytes for each pixel
        //The above only applies as long as this is a format 2 (RGB/Truecolour) png, which all the millenaire ones are.
        //Except indians. Rip. That's colour format 6 (RGBA/Truecolour) so 4 bytes per pixel. Discard the fourth.
        //Attempting to load a different kind will just error out. Probably.

        //Read some metadata about the image so we can accurately depict it
        var header = chunks["IHDR"].Data;
        var imageWidth = BitConverter.ToInt32(header.SubArray(0, 4).Reverse(), 0);
        var imageHeight = BitConverter.ToInt32(header.SubArray(4, 4).Reverse(), 0);

        var bitDepth = header[8];
        var colorFormat = header[9];

        Debug.Log("PNG Bit depth: " + bitDepth);
        Debug.Log("Color format " + colorFormat);

        //Initialize our pixel array
        var png = new RgbPixel[imageWidth, imageHeight];

        //Copy the image data to a stream for easy seeking.
        using (var imageStream = new MemoryStream(imageData))
        {
            for (var y = 0; y < imageHeight; y++)
            {
                var filterType = imageStream.ReadByte(); //For some reason every row starts with a 00 byte 

                for (var x = 0; x < imageWidth; x++)
                {
                    var one = imageStream.ReadByte();
                    var two = imageStream.ReadByte();
                    var three = imageStream.ReadByte();
                    int alpha;

                    if (colorFormat == 6)
                        alpha = imageStream.ReadByte();

                    if (filterType == 1 && x != 0)
                    {
                        var lastP = png[x - 1, y];
                        one = lastP.R + one;
                        two = lastP.G + two;
                        three = lastP.B + three;
                    }

                    if (filterType == 2 && y != 0)
                    {
                        var lastP = png[x, y - 1];
                        one = lastP.R + one;
                        two = lastP.G + two;
                        three = lastP.B + three;
                    }

                    if (filterType == 4)
                    {
                        //A, B, or C, whichever is closest to p = A + B − C
                        //Where A is to the left,
                        //B is above,
                        //And c is above and to the left
                        RgbPixel a = null;
                        RgbPixel b = null;
                        RgbPixel c = null;

                        if (x != 0)
                            a = png[x - 1, y];
                        if (y != 0)
                            b = png[x, y - 1];
                        if (x != 0 && y != 0)
                            c = png[x - 1, y - 1];

                        if (x == 0)
                        {
                            one += b.R;
                            two += b.G;
                            three += b.B;
                        } else if (y == 0)
                        {
                            one += a.R;
                            two += a.G;
                            three += a.B;
                        }
                        else
                        {
                            var Pr = a.R + b.R - c.R;
                            var Pg = a.G + b.G - c.G;
                            var Pb = a.B + b.B - c.B;

                            byte predictedR;
                            byte predictedG;
                            byte predictedB;

                            var lowestDistance = (byte) Math.Min(Math.Abs(a.R - Pr),
                                Math.Min(Math.Abs(b.R - Pr), Math.Abs(c.R - Pr)));

                            predictedR = (byte) new [] {a.R, b.R, c.R}.First(val => Math.Abs(val - Pr) == lowestDistance);
                            
                            lowestDistance = (byte) Math.Min(Math.Abs(a.G - Pg),
                                Math.Min(Math.Abs(b.G - Pg), Math.Abs(c.G - Pg)));
                            
                            predictedG = (byte) new [] {a.G, b.G, c.G}.First(val => Math.Abs(val - Pg) == lowestDistance);
                            
                            lowestDistance = (byte) Math.Min(Math.Abs(a.B - Pb),
                                Math.Min(Math.Abs(b.B - Pb), Math.Abs(c.B - Pb)));
                            
                            predictedB = (byte) new [] {a.B, b.B, c.B}.First(val => Math.Abs(val - Pb) == lowestDistance);

                            one += predictedR;
                            two += predictedG;
                            three += predictedB;
                        }
                    }

                    if (one > 255)
                        one -= 256;
                    if (two > 255)
                        two -= 256;
                    if (three > 255)
                        three -= 256;
                    
                    png[x, y] = new RgbPixel
                    {
                        R = one,
                        G = two,
                        B = three,
                    };
                }
            }
        }

        return png;
    }

    private static BuildingPlan _initFromConfig(string configPath)
    {
        var lines = File.ReadLines(configPath).Where(line => !line.StartsWith("//") && line.Trim().Length > 0);

        var plan = new BuildingPlan();

        var enumerable = lines as string[] ?? lines.ToArray();

        if (enumerable.First(line => line.StartsWith("building.length")) is string lengthConfig)
            plan.Length = int.Parse(lengthConfig.Split('=')[1]);
        else
            Debug.LogWarning("Plan config @" + configPath + " is missing building.length line.");

        if (enumerable.First(line => line.StartsWith("building.width")) is string widthConfig)
            plan.Width = int.Parse(widthConfig.Split('=')[1]);
        else
            Debug.LogWarning("Plan config @" + configPath + " is missing building.width line.");

        return plan;
    }

    public static BuildingPlan ReadFromFile(string path, string pointMapFile)
    {
        var png = ReadPng(path);

        var blockmap = new Blockmap(pointMapFile);

        var plan = _initFromConfig(path.Substring(0, path.Length - 5) + ".txt");
        var filename = Path.GetFileName(path);
        plan.Name = filename.Substring(0, filename.Length - 5);
        try
        {
            Debug.Log("PNG is " + png.GetLength(1) + " tall by " + png.GetLength(0) + " wide. Plan is supposedly " +
                      plan.Length + " tall (up-down in 2d image) and " + plan.Width + " wide.");

            var numFloors = (png.GetLength(0) + 1) / (plan.Width + 1);

            Debug.Log("Calculated number of floors: " + numFloors);

            if (numFloors != ((decimal) png.GetLength(0) + 1) / (plan.Width + 1))
                Debug.LogWarning("Plan @" + path + " appears not to have a whole number of floors? ImgWidth: " +
                                 png.GetLength(0) + ", planWidth: " + plan.Width);

            plan.NumFloors = numFloors;
            plan.Blocks = new BuildingBlock[plan.Width, numFloors, plan.Length];
            Debug.Log("Initialized building block array of size " + plan.Blocks.Length);

            for (var floor = 0; floor < numFloors; floor++)
            {
                var xOffset = (plan.Width + 1) * floor;
                //Length is up-down
                for (var rowNum = 0; rowNum < plan.Length; rowNum++)
                {
                    //Width is left-right
                    for (var columnNum = 0; columnNum < plan.Width; columnNum++)
                    {
                        //Get pixel at (plan.Width + 1) * floor, length - row
                        
                        //INVERT THE Z POS BECAUSE THE PNG IS MIRRORED. WHO KNOWS WHY.
                        var pixel = png[xOffset + columnNum, plan.Length - 1 - rowNum]; 
                        if (!blockmap.map.ContainsKey(pixel))
                        {
                            Debug.LogWarning("Blockmap does not contain entry for colour " + pixel + " at position " +
                                             (xOffset + columnNum) + ", " + rowNum);
                            CrossSceneState.UnknownColours.Add(new Vector2(xOffset + columnNum, rowNum), pixel);
                            plan.Blocks[floor, rowNum, columnNum] = new BuildingBlock("minecraft:air");
                            continue;
                        }

                        plan.Blocks[columnNum, floor, rowNum] = blockmap.map[pixel];
                    }
                }
            }

            plan.Path = path;
            return plan;
        }
        catch (Exception)
        {
            File.WriteAllBytes(path + ".IDAT", lastIdat);
            Debug.LogError(
                "Failed to parse PNG (invalid format?) Decompressed Image Data has been written to a .IDAT file in the same folder.");
            return null;
        }
    }

    private struct PngChunk
    {
        public int Length;
        public string Type;
        public byte[] Data;
        public int Checksum;
    }

    public class RgbPixel
    {
        public int R;
        public int G;
        public int B;

        public override string ToString()
        {
            return "Pixel: " + new Color(R, G, B);
        }

        public override bool Equals(object obj)
        {
            if (obj is RgbPixel other)
            {
                return other.R == R && other.G == G && other.B == B;
            }

            return false;
        }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
        {
            return R << 16 + G << 8 + B;
        }
    }
}