public class BuildingPlan
{
    public string Name;
    public BuildingBlock[,,] Blocks;
    public int Length;
    public int Width;
    public int NumFloors;
    public string Path;
}