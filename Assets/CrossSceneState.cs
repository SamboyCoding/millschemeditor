using System.Collections.Generic;
using UnityEngine;

public static class CrossSceneState
{
    public static BuildingPlan CurrentPlan;
    
    public static Dictionary<Vector2, PngPlanReader.RgbPixel> UnknownColours = new Dictionary<Vector2, PngPlanReader.RgbPixel>();

    public static string GameJarPath = string.Empty;

    public static bool ModelsFinishedResolving = false;

    private static BlockScript _selectedBlock; 

    public static BlockScript SelectedBlock
    {
        get => _selectedBlock;
        set
        {
            _selectedBlock = value;
            Camera.main.GetComponent<EditorUiScript>().UpdateRightPanel(); 
        }
    }
}