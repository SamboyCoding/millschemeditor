using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class Blockmap
{
    public readonly Dictionary<PngPlanReader.RgbPixel, BuildingBlock> map =
        new Dictionary<PngPlanReader.RgbPixel, BuildingBlock>();

    public Blockmap(string path)
    {
        //Get the meaningful lines
        var lines = File.ReadAllLines(path).Where(line => !line.StartsWith("//") && line.Trim().Length > 0);

        foreach (var line in lines)
        {
            try
            {
                var split = line.Split(';');
                if (split.Length < 5) continue;

                var name = split[0];
                var blockId = split[1];
                var stateInfo = split[2];
                var blockState = new Dictionary<string, string>();
                short meta = 0;
                try
                {
                    meta = short.Parse(stateInfo);
                }
                catch (FormatException)
                {
                    var stateSplit = stateInfo.Split(',');
                    foreach (var stateKvp in stateSplit)
                    {
                        var kvpSplit = stateKvp.Split('=');
                        blockState[kvpSplit[0]] = kvpSplit[1];
                    }
                }

                var colors = split[4].Split('/');

                var pixel = new PngPlanReader.RgbPixel
                {
                    R = int.Parse(colors[0]),
                    G = int.Parse(colors[1]),
                    B = int.Parse(colors[2]),
                };
                var block = new BuildingBlock(name, blockId, meta) {BlockStateData = blockState};
                
                map.Add(pixel, block);
                
                
            }
            catch (Exception e)
            {
                Debug.LogError("Exception handling line " + line + " - " + e);
            }
        }

        Debug.Log("Loaded a blockmap with " + map.Count + " blocks defined.");
        var assetPath = Path.Combine(Application.persistentDataPath, "GameVerCache", "1.12.2", "assets", "minecraft");
    }
}