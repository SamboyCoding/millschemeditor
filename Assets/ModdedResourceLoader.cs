using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ionic.Zip;
using UnityEngine;

public static class ModdedResourceLoader
{
    public static string ModVerCachePath;
    private static Dictionary<string, string> _modFolders = new Dictionary<string, string>();

    public static void RegisterPath(string modId, string path)
    {
        _modFolders.Add(modId, path);
        Debug.Log("Linked " + modId + " to folder " + path);
    }

    public static void RegisterModJar(string modId, string modJarPath)
    {
        if (!Directory.Exists(ModVerCachePath))
            Directory.CreateDirectory(ModVerCachePath);

        if (!File.Exists(modJarPath))
        {
            Debug.LogError("Cannot register mod " + modId + " as being at " + modJarPath + " as the path doesn't exist!");
            return;
        }

        var destinationFolder = Path.Combine(ModVerCachePath, Path.GetFileNameWithoutExtension(modJarPath));
        if (Directory.Exists(destinationFolder))
        {
            RegisterPath(modId, destinationFolder);
            return;
        }

        Directory.CreateDirectory(destinationFolder);
        Debug.Log("Extracting " + modJarPath + " => " + destinationFolder);

        using (var modJar = ZipFile.Read(modJarPath))
        {
            var assets = from e in modJar.Entries
                where e.FileName.StartsWith("assets/" + modId + "/models") ||
                      e.FileName.StartsWith("assets/" + modId + "/textures") ||
                      e.FileName.StartsWith("assets/" + modId + "/blockstates")
                select e;

            foreach (var asset in assets)
                asset.Extract(destinationFolder);
        }
        
        RegisterPath(modId, destinationFolder);
    }

    public static string GetBlockStatePath(BuildingBlock block, string preprocessedName)
    {
        var modId = block.RegistryName.Substring(0, block.RegistryName.IndexOf(":", StringComparison.Ordinal) );
        if (!_modFolders.ContainsKey(modId))
        {
            Debug.LogWarning("Cannot get block state path for " + block + " as its mod id, " + modId + " is not registered to a directory.");
            return null;
        }

        var resourcesRoot = _modFolders[modId];
        var blockstateRoot = Path.Combine(resourcesRoot, "assets", modId, "blockstates");
        if (!Directory.Exists(blockstateRoot))
        {
            Debug.LogWarning("Missing directory: " + blockstateRoot);
            return null;
        }

        if (File.Exists(Path.Combine(blockstateRoot, preprocessedName + ".json")))
        {
            return Path.Combine(blockstateRoot, preprocessedName + ".json");
        }
        
        Debug.LogError("Unable to locate " + preprocessedName + " in the blockstate folder.");
        return null;
    }

    public static string ResolveModelFilePath(string modelFileName)
    {
        var modId = modelFileName.Substring(0, modelFileName.IndexOf(":", StringComparison.Ordinal) );
        if (!_modFolders.ContainsKey(modId))
        {
            Debug.LogWarning("Cannot get model file path for " + modelFileName + " as its mod id, " + modId + " is not registered to a directory.");
            return null;
        }
        
        var resourcesRoot = _modFolders[modId];
        
        var modelsRoot = Path.Combine(resourcesRoot, "assets", modId, "models", "block");
        var fileName = modelFileName.Substring(modelFileName.IndexOf(":", StringComparison.Ordinal) + 1);
        
        if(fileName.Contains("/"))
            return Path.Combine(resourcesRoot, "assets", modId, "models", fileName + ".json");

        return Path.Combine(modelsRoot, fileName + ".json");
    }

    public static string ResolveTextureFilePath(string partialPath)
    {
        //PartialPath is in the format modid:blocks/image.png
        var modId = partialPath.Substring(0, partialPath.IndexOf(":", StringComparison.Ordinal) );
        var fileName = partialPath.Substring(partialPath.IndexOf(":", StringComparison.Ordinal) + 1);
        
        if (modId == "minecraft")
        {
            return VanillaResourceLoader.ResolveTexturePath(fileName);
        }
        if (!_modFolders.ContainsKey(modId))
        {
            Debug.LogWarning("Cannot get image " + partialPath + " as its mod id, " + modId + " is not registered to a directory.");
            return null;
        }
        
        var resourcesRoot = _modFolders[modId];
        
        var texturesRoot = Path.Combine(resourcesRoot, "assets", modId, "textures");
        

        return Path.Combine(texturesRoot, fileName + ".png");
    }
}